jQuery(document).ready(function ($) {
	var body = $('body');

	$(".log-btn").click(function() {
		$(".popup-login").css("display", "block");
		body.css("overflow-y", "hidden");
		$(window).scrollTop(0);
	});

	$(".popup-cancel").click(function() {
		$(".popup-login").css("display", "none");
		body.css("overflow-y", "auto");
	});
});
