    <?php require('./header.php') ?>
    <?php require('slider.php') ?>
    <div class="content">
        <div class="maindiv"> 
        <?php 
			require_once('connect.php');
			$last_news = mysqli_query($connect, "SELECT * FROM news ORDER BY id_news DESC LIMIT 8");
			$count = mysqli_num_rows($last_news);

			for ($i = 0; $i < $count; $i++) {
				mysqli_data_seek($last_news, $i);
				$row = mysqli_fetch_array($last_news, MYSQLI_ASSOC);
		?>

		<div class="news-content">
                    <h2> <a href="news-full.php?id=<?php echo $row['id_news']; ?>"> <?php echo $row['title']; ?> </a> </h2>  
                    <p> <?php echo $row['intro_text'];?> </p>
            </div>

        <?php } ?>
        </div>
        <?php require('./sidebar.php') ?>
    </div>
    <?php require('./footer.php') ?>
