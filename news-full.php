 <?php require('./header.php') ?>
 <?php require('./slider.php') ?>
    <div class="content">
        <div class="maindiv"> 
            <div class="news-view">
                <?php 
                    require_once('connect.php');
                    $id = $_GET['id'];

                    $str = mysqli_query($connect, "SELECT * FROM news WHERE id_news = $id");
                    $row = mysqli_fetch_array($str, MYSQLI_ASSOC);
                ?>
                <h1> <?php echo $row['title']; ?> </h1>
                <p class="news-text">
                    <?php echo $row['intro_text'];?>
                </p>
                <hr>
            </div>
        </div>
        <?php require('./sidebar.php') ?>

    </div>
    <?php require('./footer.php') ?>
