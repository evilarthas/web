<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
    <title> News </title>
</head>
<body>
    <?php
        require('connect.php');
        session_start();
    ?>
    <div class="header">
        <div class="header-inner">
            <h1> <a href="./index.php"> GamesCom </a> </h1>
        </div>
    </div>
    <div class="menu">
        <ul>
            <li> <a href="index.php"> Главная </a> </li>
            <li> <a href="news.php"> Новости </a> </li>
            <li> <a href="contacts.php"> Контакты </a> </li>
            <li> 
                <div class="dropdown">
                  <button class="dropbtn"> Категории </button>
                  <div class="dropdown-content">
                    <a href="./news-view.php?category=<?php echo "action"; ?>"> Экшн </a>
                    <a href="./news-view.php?category=<?php echo "shooter"; ?>"> Шутеры </a> 
                    <a href="./news-view.php?category=<?php echo "simulator"; ?>"> Стратегии </a> 
                    <a href="./news-view.php?category=<?php echo "strategy"; ?>"> Симуляторы </a> 
                  </div>
                </div>
            <?php if(!isset($_SESSION["id_user"])) { ?>
                <li> <button class="log-btn"> Аккаунт </button> </li>
            <?php } else { ?>
                <li> <a href="all-users.php"> [ все пользователи ] </a> </li>
                <li> <a href="all-news.php"> [ все новости ] </a> </li>
            <?php } ?>
        </ul>
    </div>