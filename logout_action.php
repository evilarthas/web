<?php
	session_start();
	unset($_SESSION["login"]);
	unset($_SESSION["first_name"]);
	unset($_SESSION["second_name"]);
	unset($_SESSION["id_user"]);
	header("Location:index.php");
?>