 <?php require('./header.php') ?>
 <?php require('./slider.php') ?>
    <div class="content">
        <div class="maindiv"> 
            <?php require_once('connect.php'); ?>
            <h2 style="color: yellow"> Все пользователи </h2>
            <?php $str = mysqli_query($connect, "SELECT * FROM users"); ?>
            
            <table class="db_table">
            <tr>
                <td> ID </td>
                <td> Логин </td>
                <td> Имя </td>
                <td> Фамилия </td>
            </tr>
                <?php while($user = mysqli_fetch_array($str)) { ?>
            <tr>
                <td> <?php echo $user['id_user']; ?> </td>
                <td> <?php echo $user['login']; ?> </td>
                <td> <?php echo $user['first_name']; ?> </td>
                <td> <?php echo $user['second_name']; ?> </td>
                <td> <a href="./edit-user.php?id=<?php echo $user['id_user']; ?>"> [ изменить ] </a> </td>
                <td> <a style="color: red" href="./delete_user_action.php?id=<?php echo $user['id_user']; ?>">  [ удалить ] </a> </td>
            </tr>
            <?php } ?>
        </table>
        </div>
        <?php require('./sidebar.php') ?>

    </div>
    <?php require('./footer.php') ?>
